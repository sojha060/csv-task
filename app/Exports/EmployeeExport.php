<?php

namespace App\Exports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmployeeExport implements FromCollection,WithHeadings
{
    public $data;
    public function __construct(){
    }


    public function headings():array{
        return [
            'ID',
            'Name',
            'Designation',
            'JoinDate'
        ];
    }



    public function collection()
    {
        return collect(Employee::getEmployee());
    }
}
