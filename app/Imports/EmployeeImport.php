<?php

namespace App\Imports;

use App\Models\Employee;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;

class EmployeeImport implements WithEvents
{
    use Importable, RegistersEventListeners;

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        // return [
        //     '3','Surendra','Software Engineer','8/2/2021'
        // ]


        return collect(Employee::getEmployee());

    }
}
