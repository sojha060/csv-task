<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table="employees";

    public static function getEmployee(){
        $informations = Employee::select('id','name','designation','joindate')->get()->toArray();

        return $informations;
    }



    


}
