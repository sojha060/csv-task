<?php

namespace App\Http\Controllers;

use App\Exports\EmployeeExport;
use App\Imports\EmployeeImport;
use App\Models\Employee;
use Exception;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;
use PhpParser\Node\Stmt\While_;
use Spatie\SimpleExcel\SimpleExcelWriter;



// =  storage_path('employee_list.csv');;


class EmployeeController extends Controller
{
    public $path;

    public $temp_path;

    public $rules;



    public function __construct()
    {
        $this->path = storage_path('employee_list.csv');
        $this->temp_path = storage_path('employee_list_copy.csv');

        $this->rules = [
            'employee_name' => 'required',
            'designation' => 'required',
            'joindate' => 'required'
        ];
    }



    public function index()
    {

        $table = fopen($this->path, 'r');

        $informations = Employee::orderBy('id')->get();

        return view(
            'index',
            compact('table', 'informations')
        );
    }


    public function create()
    {
        return view('create');
    }




    public function exportCsv()
    {
        return Excel::download(new EmployeeExport, 'employee_list.csv');
    }


    public function append(Request $request)
    {

        // save data to database

        $request->validate($this->rules);

        $information = new Employee();

        $information->name = $request->employee_name;

        $information->designation = $request->designation;

        $information->joindate = $request->joindate;

        $information->save();


        $path = storage_path('employee_list.csv');

        $array = [
            $information->id, $information->name, $information->designation, $information->joindate
        ];





        try {
            $handle = fopen($path, "a");
        } catch (Exception $e) {
            // dd('Please close the excel file before modifying it');

            return redirect()->back()->with('message', 'Please close the excel file before modifying it!!');
        }

        fputcsv($handle, $array); # $line is an array of strings (array|string[])

        fclose($handle);


        return redirect()->route('employee.home')->with('message', 'Employee record added to csv successfully!!');
    }

    public function deleteFromExcel($ID)
    {
        $search = $ID;

        try {
            $table = fopen($this->path, 'r+');
            $temp_table = fopen($this->temp_path, 'w');
        } catch (Exception $e) {
            return redirect()->back()->with('message', 'Please Close CSV the file before deleting the row');
        }



        $id = $ID; // the name of the column you're looking for

        while (($data = fgetcsv($table, 1000)) !== FALSE) {
            if (reset($data) == $id) { // this is if you need the first column in a row
                continue;
            }
            fputcsv($temp_table, $data);
        }
        fclose($table);
        fclose($temp_table);







        rename($this->temp_path, $this->path);

        // delete data from database

        $information = Employee::find($search)->delete();

        return redirect()->route('employee.home')->with('message', 'Employee record deleted from csv successfully!!');
    }


    public function editFromExcel(Request $request)
    {


        $request->validate($this->rules);


        try {
            $handle = fopen($this->path, "r");
            $fp = fopen($this->temp_path, 'w');
        } catch (Exception $e) {
            return redirect()->back()->with('message', 'Please Close CSV the file before updating the file');
        }


        $information = Employee::find($request->id);
        $information->name = $request->employee_name;
        $information->designation = $request->designation;
        $information->joindate = $request->joindate;
        $information->save();

        $edit_id = $request->id;


        $primary_key = Employee::pluck('id');

        $n = Employee::count();

        // dd($primary_key);

        $i = 0;
        $newdata = [];
        while (($data = fgetcsv($handle, $n, ",")) !== FALSE) {

            if ($i < $n) {
                // UPDATE 100TH ROW DATA (TO EXCLUDE, KEEP ONLY $i++ AND continue)
                if ($primary_key[$i] == $edit_id) {
                    $newdata[$i][] = $information->id;
                    $newdata[$i][] = $request->employee_name;
                    $newdata[$i][] = $information->designation;
                    $newdata[$i][] = $information->joindate;
                    $i++;
                    continue;
                }
                $newdata[$i] = $data;

                $i++;
            }
        }


        // dd($newdata);

        fclose($handle);

        array_unshift($newdata ,['ID','Employee Name','Designation','Joindate'] );


        // EXPORT CSV
        foreach ($newdata as $rows) {
            fputcsv($fp, $rows);
        }

        fclose($fp);

        rename($this->temp_path, $this->path);


        return redirect()->route('employee.home')->with('message', 'Employee record updated successfully!!');
    }


    public function editForm($id)
    {

        $information = Employee::find($id);

        return view('edit', compact('id', 'information'));
    }
}
