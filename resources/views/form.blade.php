<div class="col-md-6">
    <div class="form-group">

        {{ Form::text('employee_name',@$information->name,['class'=>'form-control','id'=>'name','placeholder'=>'Employee Name']) }}
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        {{ Form::text('designation',null,['class'=>'form-control','placeholder'=>'Designation']) }}

    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        {{ Form::date('joindate',null,['class'=>'form-control','placeholder'=>'Join Date']) }}

    </div>
</div>
