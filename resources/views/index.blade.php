

@extends('main')


@section('title')
CSV Content
@endsection


@section('content')

    <div class="row">

        <div class="col-md-12">

            <a href="{{ route('create.csv') }}" class="btn btn-success">Append To CSV File</a>

            <table class="table">
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>JoinDate</th>
                    <th>Action</th>
                </thead>

                <tbody>

                    @foreach ($informations as $value)
                    <tr>
                        <td>{{ $value->id }}</td>

                        <td>{{ $value->name }}</td>
                        <td>{{ $value->designation }}</td>
                        <td>{{ $value->joindate }}</td>
                        <td>
                            <form action="{{ route('delete.csv',$value->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure you want to delete data from csv file?')">Delete</button>
                            </form>


                            <a class="btn btn-success" href="{{ route('edit.csv',$value->id) }}">Edit</a>
                        </td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>









@endsection











