@extends('main')


@section('title')
Update Record
@endsection


@section('content')

{{ Form::model($information,['route'=>['update.csv'],'name'=>'contactForm','method'=>'post']) }}


    <input type="hidden" name="id" value="{{ $id }}">
    @csrf
    <div class="row">
        @include('form')

        <div class="col-md-12">
            <div class="form-group">
                <input type="submit" value="Update TO CSV" class="btn btn-primary">
                <div class="submitting"></div>
            </div>
        </div>
    </div>







{{ Form::close() }}

@endsection
