@extends('main')


@section('title')
Add Record
@endsection


@section('content')
<form action="{{ route('append.csv') }}" method="get"  name="contactForm">
    @csrf
    <div class="row">
        @include('form')

        <div class="col-md-12">
            <div class="form-group">
                <input type="submit" value="Add TO CSV" class="btn btn-primary">
                <div class="submitting"></div>
            </div>
        </div>
    </div>







</form>


@endsection






