<?php

use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',[EmployeeController::class,'index'])->name('employee.home');



Route::get('create',[EmployeeController::class,'create'])->name('create.csv');

Route::get('employee-csv',[EmployeeController::class,'exportCsv'])->name('employee.csv');

Route::get('append-csv',[EmployeeController::class,'append'])->name('append.csv');

Route::delete('delete-csv/{ID}',[EmployeeController::class,'deleteFromExcel'])->name('delete.csv');


Route::get('edit-csv/{id}',[EmployeeController::class,'editForm'])->name('edit.csv');



Route::post('update-csv',[EmployeeController::class,'editFromExcel'])->name('update.csv');


